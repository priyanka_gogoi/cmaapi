﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MX.Internship2021.CMA.ExceptionLayer
{
    public class ContactExceptions 
    {
        public class IncompleteCompanyDetailsException : Exception
        {
            public IncompleteCompanyDetailsException(string message) : base(message) { }
        }

        public class MandatoryFieldsEmptyException : Exception
        {
            public MandatoryFieldsEmptyException(string message) : base(message) { }
        }

        public class WrongPhoneNumberFormatException : Exception
        {
            public WrongPhoneNumberFormatException(string message) : base(message) { }
        }

        public class InvalidEmailException : Exception
        {
            public InvalidEmailException(string message) : base(message) { }
        }

        public class InvalidDateOfBirthException : Exception
        {
            public InvalidDateOfBirthException(string message) : base(message) { }
        }
    }
}
