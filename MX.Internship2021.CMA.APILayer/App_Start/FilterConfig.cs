﻿using System.Web;
using System.Web.Mvc;

namespace MX.Internship2021.CMA.APILayer
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
