﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace MX.Internship2021.CMA.APILayer.Controllers
{
    public class ContactsController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET api/values
        [HttpGet]
        public HttpResponseMessage GetContacts()
        {
            try
            {
                log.Info("Call to get all contacts data");
                IEnumerable<Entities.Contact> contactList = new BusinessLayer.Contact().GetAllContacts();
                log.Info("Received all contacts successfully");
                return Request.CreateResponse(HttpStatusCode.OK, contactList);
            }
            catch (Exception)
            {
                log.Error("Contact data was not received");
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new Entities.JsonResponse
                    {
                        StatusCode = 500,
                        Status = "InternalServerError",
                        Message = "There was an error receiving all contacts"
                    });
            }
        }

        // GET api/values/5
        [HttpGet]
        public HttpResponseMessage GetSingleContacts(int id)
        {
            try
            {
                log.Debug($"Call to receive data of contact with Id = {id}");
                Entities.Contact contactDetails = new BusinessLayer.Contact().GetContactDetails(id);
                log.Info("Data received successfully");
                return Request.CreateResponse(HttpStatusCode.OK, contactDetails);
            }
            catch (ArgumentOutOfRangeException)
            {
                log.Error($"Contact with Id = {id} does not exist");
                return Request.CreateResponse(HttpStatusCode.NotFound, 
                    new Entities.JsonResponse 
                    { 
                        StatusCode = 404,
                        Status = "NotFound",
                        Message = $"Contact id = {id} does not exist"
                    });
            }
            catch (Exception ex)
            {
                log.Error($"Data of id = {id} not received");
                // return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new Entities.JsonResponse
                    {
                        StatusCode = 500,
                        Status = "InternalServerError",
                        Message = ex.Message
                    });
            }
        }

        // GET api/organizations
        [Route("api/organizations")]
        [HttpGet]
        public HttpResponseMessage GetOrganizationList()
        {
            try
            {
                log.Info("Call to get organization list");
                List<string> orgList = new BusinessLayer.Contact().OrganizationList();
                log.Info("Received organization list successfully");
                return Request.CreateResponse(HttpStatusCode.OK, orgList);
            }
            
            catch (Exception ex)
            {
                log.Error("Organization list was not received");
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new Entities.JsonResponse
                    {
                        StatusCode = 500,
                        Status = "InternalServerError",
                        Message = ex.Message
                    });
            }
        }

        // POST api/values
        [HttpPost]
        public HttpResponseMessage AddContact([FromBody] Entities.Contact contact)
        {
            try
            {
                log.Info("Call to add new contact into database");
                int createdId = new BusinessLayer.Contact().CreateNewContact(contact);
                log.Debug($"Contact was created successfully with id = {createdId}");
                //return Request.CreateResponse(HttpStatusCode.OK, $"Contact with id = {createdId} was created");
                return Request.CreateResponse(HttpStatusCode.OK,
                    new Entities.JsonResponse
                    {
                        StatusCode = 200,
                        Status = "Success",
                        Message = $"Contact with id = {createdId} was created"
                    });
            }
            catch (Exception)
            {
                log.Error("Adding contact request was not successful");
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new Entities.JsonResponse
                    {
                        StatusCode = 500,
                        Status = "InternalServerError",
                        Message = "Name, PhoneNo, PhoneCode, DateOfBirth and ContactImage are mandatory"
                    });
            }
        }

        // PUT api/values/5
        [HttpPut]
        public HttpResponseMessage UpdateContact(int id, [FromBody] Entities.Contact editedContact)
        {
            try
            {
                int rowsAffected = new BusinessLayer.Contact().EditContact(editedContact, id);
                log.Debug($"Contact was updated successfully with id = {id}");
                //return Request.CreateResponse(HttpStatusCode.OK, rowsAffected);
                return Request.CreateResponse(HttpStatusCode.OK,
                    new Entities.JsonResponse
                    {
                        StatusCode = 200,
                        Status = "Success",
                        Message = $"Contact with id = {id} edited successfully"
                    });
            }
            catch (ArgumentOutOfRangeException)
            {
                log.Debug($"Contact with id = {id} does not exist");
                //return Request.CreateResponse(HttpStatusCode.NotFound, $"Id = {id} does not exist");
                return Request.CreateResponse(HttpStatusCode.NotFound,
                    new Entities.JsonResponse
                    {
                        StatusCode = 404,
                        Status = "NotFound",
                        Message = $"Contact id = {id} does not exist"
                    });
            }
            catch (Exception)
            {
                //return Request.CreateResponse(HttpStatusCode.InternalServerError, "Name, PhoneNo, PhoneCode, DateOfBirth and ContactImage are mandatory");
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new Entities.JsonResponse
                    {
                        StatusCode = 500,
                        Status = "InternalServerError",
                        Message = "Name, PhoneNo, PhoneCode, DateOfBirth and ContactImage are mandatory"
                    });
            }
        }

        // DELETE api/values/5
        [HttpDelete]
        public HttpResponseMessage DeleteSingleContact(int id)
        {
            try
            {
                new BusinessLayer.Contact().DeleteContact(id);
                //return new HttpResponseMessage(HttpStatusCode.OK);
                return Request.CreateResponse(HttpStatusCode.OK,
                    new Entities.JsonResponse
                    {
                        StatusCode = 200,
                        Status = "Success",
                        Message = $"Contact with id = {id} was deleted"
                    });
            }
            catch (ArgumentOutOfRangeException)
            {
                log.Debug($"Contact with id = {id} does not exist");
                //return Request.CreateResponse(HttpStatusCode.NotFound, $"Id = {id} does not exist");
                return Request.CreateResponse(HttpStatusCode.NotFound,
                    new Entities.JsonResponse
                    {
                        StatusCode = 404,
                        Status = "NotFound",
                        Message = $"Contact id = {id} does not exist"
                    });
            }
            catch (Exception)
            {
                //return Request.CreateResponse(HttpStatusCode.InternalServerError);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new Entities.JsonResponse
                    {
                        StatusCode = 500,
                        Status = "InternalServerError",
                        Message = "Error in the getting response"
                    });
            }
        }
    }
}
