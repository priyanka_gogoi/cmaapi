﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MX.Internship2021.CMA.DataLayer
{
    class CMADBQueryBuilder
    {
        public SqlCommand InsertQueryBuilder(Entities.Contact contact, SqlConnection sqlConnection)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into ContactTable");
            sb.Append("(name, phoneCode, phoneNo, email, address, city, country, ");
            sb.Append("dateofbirth, year, socialName, socialHandle, relationship, ");
            sb.Append("companyName, position, contactImage) values");
            sb.Append("(@Name, @PhoneCode, @PhoneNo, @Email, @Address, @City, @Country, ");
            sb.Append("@DateOfBirth, @Year, @SocialName, @SocialHandle, @Relationship, ");
            sb.Append("@CompanyName, @Position, @ContactImage);");
            sb.Append("select @@identity as IdNumber;");

            using (SqlCommand insertCommand = new SqlCommand(sb.ToString(), sqlConnection))
            {
                try
                {
                    insertCommand.Parameters.AddWithValue("@Name", contact.Name);
                    insertCommand.Parameters.AddWithValue("@PhoneCode", contact.PhoneCode);
                    insertCommand.Parameters.AddWithValue("@PhoneNo", contact.PhoneNo.Trim());
                    insertCommand.Parameters.AddWithValue("@Email", contact.Email);
                    insertCommand.Parameters.AddWithValue("@Address", contact.Address);
                    insertCommand.Parameters.AddWithValue("@City", contact.City);
                    insertCommand.Parameters.AddWithValue("@Country", contact.Country);
                    insertCommand.Parameters.AddWithValue("@DateOfBirth", Convert.ToDateTime(contact.DateOfBirth));
                    insertCommand.Parameters.AddWithValue("@Year", Convert.ToInt32(contact.Year));
                    insertCommand.Parameters.AddWithValue("@SocialName", contact.SocialName);
                    insertCommand.Parameters.AddWithValue("@SocialHandle", contact.SocialHandle);
                    insertCommand.Parameters.AddWithValue("@Relationship", contact.Relationship);
                    insertCommand.Parameters.AddWithValue("@ContactImage", contact.ContactImage);
                    insertCommand.Parameters.AddWithValue("@CompanyName", contact.CompanyName);
                    insertCommand.Parameters.AddWithValue("@Position", contact.Position);

                    return insertCommand;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public SqlCommand UpdateQueryBuilder(int id, Entities.Contact contact, SqlConnection sqlConnection)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("update ContactTable set name=@Name, phoneCode=@PhoneCode, ");
            sb.Append("phoneNo=@PhoneNo, " + "email=@Email, address=@Address, city=@City, ");
            sb.Append("country=@Country, dateofbirth=@DateOfBirth, ");
            sb.Append("year=@Year, socialName=@SocialName, socialHandle=@SocialHandle, ");
            sb.Append("relationship=@Relationship, " + "contactImage=@ContactImage, ");
            sb.Append("companyName=@CompanyName, position=@Position ");
            sb.Append("where id=@Id");

            using (SqlCommand updateCommand = new SqlCommand(sb.ToString(), sqlConnection))
            {
                try
                {
                    updateCommand.Parameters.AddWithValue("@Name", contact.Name);
                    updateCommand.Parameters.AddWithValue("@PhoneCode", contact.PhoneCode);
                    updateCommand.Parameters.AddWithValue("@PhoneNo", contact.PhoneNo);
                    updateCommand.Parameters.AddWithValue("@Email", contact.Email);
                    updateCommand.Parameters.AddWithValue("@Address", contact.Address);
                    updateCommand.Parameters.AddWithValue("@City", contact.City);
                    updateCommand.Parameters.AddWithValue("@Country", contact.Country);
                    updateCommand.Parameters.AddWithValue("@DateOfBirth", contact.DateOfBirth);
                    updateCommand.Parameters.AddWithValue("@Year", Convert.ToInt32(contact.Year));
                    updateCommand.Parameters.AddWithValue("@SocialName", contact.SocialName);
                    updateCommand.Parameters.AddWithValue("@SocialHandle", contact.SocialHandle);
                    updateCommand.Parameters.AddWithValue("@Relationship", contact.Relationship);
                    updateCommand.Parameters.AddWithValue("@ContactImage", contact.ContactImage);
                    updateCommand.Parameters.AddWithValue("@CompanyName", contact.CompanyName);
                    updateCommand.Parameters.AddWithValue("@Position", contact.Position);
                    updateCommand.Parameters.AddWithValue("@Id", id);

                    return updateCommand;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public SqlCommand DeleteQueryBuilder(int id, SqlConnection sqlConnection)
        {
            StringBuilder sb = new StringBuilder("delete from ContactTable where Id = @Id");
            using (SqlCommand deleteCommand = new SqlCommand(sb.ToString(), sqlConnection))
            {
                deleteCommand.Parameters.AddWithValue("@Id", id);
                return deleteCommand;
            }
        }

        public SqlCommand GetBirthdaysQueryBuilder(SqlConnection sqlConnection)
        {
            StringBuilder sb = new StringBuilder("select top 5 name, ");
            sb.Append("(case when DATEDIFF(dd, CONCAT(year, '-', month(CURRENT_TIMESTAMP), '-', day(CURRENT_TIMESTAMP)), dateofbirth) > 0 ");
            sb.Append("then CONCAT(DATEDIFF(dd, CONCAT(year, '-', month(CURRENT_TIMESTAMP), '-', day(CURRENT_TIMESTAMP)), dateofbirth), 'Days') ");
            sb.Append("when DATEDIFF(dd, CONCAT(year, '-', month(CURRENT_TIMESTAMP), '-', day(CURRENT_TIMESTAMP)), dateofbirth) = 0 ");
            sb.Append("then 'Happy BirthDay!!' ");
            sb.Append("else CONCAT(DATEDIFF(dd, CONCAT(year - 1, '-', month(CURRENT_TIMESTAMP), '-', day(CURRENT_TIMESTAMP)), dateofbirth), 'Days') end) ");
            sb.Append("as daysleft, (case when DATEDIFF(dd, CONCAT(year, '-', month(CURRENT_TIMESTAMP), '-', day(CURRENT_TIMESTAMP)), dateofbirth) > 0 ");
            sb.Append("then DATEDIFF(dd, CONCAT(year, '-', month(CURRENT_TIMESTAMP), '-', day(CURRENT_TIMESTAMP)), dateofbirth) ");
            sb.Append("when DATEDIFF(dd, CONCAT(year, '-', month(CURRENT_TIMESTAMP), '-', day(CURRENT_TIMESTAMP)), dateofbirth) = 0 ");
            sb.Append("then 0 else DATEDIFF(dd, CONCAT(year - 1, '-', month(CURRENT_TIMESTAMP), '-', day(CURRENT_TIMESTAMP)), dateofbirth) end) ");
            sb.Append("as orderColumn from ContactTable order by orderColumn");

            SqlCommand getBirthdaysCommand = new SqlCommand(sb.ToString(), sqlConnection);
            return getBirthdaysCommand;
        }
    }
}
