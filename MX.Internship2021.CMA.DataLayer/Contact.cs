﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using MX.Internship2021.CMA.Entities;
using System.Data;
using log4net;

namespace MX.Internship2021.CMA.DataLayer
{
    public class Contact
    {
        private SqlConnection conn = new SqlConnection(Properties.Settings.Default.DBConnString);
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private int NoReturnQueryExecution(SqlCommand sqlCommand)
        {
            try
            {
                conn.Open();
                int rowsAffected = sqlCommand.ExecuteNonQuery();
                conn.Close();
                return rowsAffected;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                throw;
            }
            finally
            {
                conn.Close();
            }
        }

        private DataTable ReturnDatatableQueryExecution(SqlCommand command)
        {
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);

                return dataTable;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                throw;
            }
        }

        public int CreateNewRecord(Entities.Contact contact)
        {
            try
            {
                SqlCommand command = new CMADBQueryBuilder().InsertQueryBuilder(contact, conn);

                log.Info("Executing insert statement on the database");
                DataTable dt = ReturnDatatableQueryExecution(command);
                log.Info("Record created successfully.");

                int newlyCreatedId;
                newlyCreatedId = Convert.ToInt32(dt.Rows[0]["IdNumber"]);
                return newlyCreatedId;
            }
            catch (Exception ex)
            {
                log.Error("Problem creating new record. " + ex.Message);
                throw ex;
            }
        }

        public int EditRecord(Entities.Contact contact, int id)
        {
            try
            {
                SqlCommand command = new CMADBQueryBuilder().UpdateQueryBuilder(id, contact, conn);

                log.Info("Executing update statement to edit row values in the database");

                int rowsAffected = NoReturnQueryExecution(command);                
                log.Info("Record edited successfully");

                return rowsAffected;
            }
            catch (Exception ex)
            {
                log.Error("Editing record unsuccessful. " + ex.Message);
                throw new Exception("There was a problem updating the record", ex);
            }
        }

        public Entities.Contact GetRecord(int id)
        {
            string query = "select * from ContactTable where id = @Id";

            try
            {
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@Id", id);
                log.Debug("Getting column values of row with email '" + id + "' from database table and filling it into DataTable");

                DataTable dataTable = ReturnDatatableQueryExecution(command);
                Entities.Contact retrievedContact = new Entities.Contact().GetListFromDataTable(dataTable)[0];
                log.Debug($"Attributes of contact with name {retrievedContact.Name} was retrieved");

                return retrievedContact;
            }
            catch(ArgumentOutOfRangeException ex)
            {
                log.Error(ex.Message);
                throw new ArgumentOutOfRangeException($"Id = {id} does not exist.", ex);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                throw new Exception("Record could not be fetched due to internal issue.", ex);
            }
        }

        public List<Entities.Contact> GetAllRecords()
        {
            string query = "select * from ContactTable";
            try
            {
                SqlCommand command = new SqlCommand(query, conn);
                log.Info("Getting all records from database table and filling it into DataTable");

                DataTable dataTable = ReturnDatatableQueryExecution(command);
                log.Info("Records retrieved successfully into DataTable");
                log.Info("Converting DataTable into List of Contacts");
                List<Entities.Contact> allContacts = new Entities.Contact().GetListFromDataTable(dataTable);

                return allContacts;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                throw new Exception("There was a problem fetching all data records.", ex);
            }
        }

        public void DeleteRecord(int id)
        {
            try
            {
                log.Debug("Deleting row with email '" + id + "' from database table");
                SqlCommand commamd = new CMADBQueryBuilder().DeleteQueryBuilder(id, conn);
                log.Info("Row deleted successfully");

                int rowsAffected = NoReturnQueryExecution(commamd);
                if(rowsAffected == 0)
                {
                    log.Error($"Id = {id} does not exist");
                    throw new ArgumentOutOfRangeException($"Id = {id} does not exist.");
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                log.Error(ex.Message);
                throw ex;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                throw new Exception("Record could not be fetched due to internal issue.", ex);
            }
        }

        public void DeleteAllRecords()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("delete from ContactTable", conn);
                log.Info("Deleting all rows from database table");

                NoReturnQueryExecution(cmd);
                log.Info("All rows were deleted");
            }
            catch (Exception ex)
            {
                log.Error("Query to be executed is wrong. " + ex.Message);
                throw new Exception("Contacts could not be deleted due to internal error", ex);
            }
        }

        public DataTable ReturnBirthdays()
        {
            try
            {
                SqlCommand command = new CMADBQueryBuilder().GetBirthdaysQueryBuilder(conn);
                log.Debug("Getting DOB column values from database table and filling it into DataTable");
                DataTable birthdayData = ReturnDatatableQueryExecution(command);

                return birthdayData;
            }
            catch(Exception ex)
            {
                log.Error(ex.Message);
                throw new Exception("There was a problem fetching birthday data.", ex);
            }
        }
    }
}