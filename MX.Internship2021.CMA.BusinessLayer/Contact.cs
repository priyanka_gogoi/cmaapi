﻿using System;
using System.Collections.Generic;
using System.Data;
using log4net;

namespace MX.Internship2021.CMA.BusinessLayer
{
    public class Contact
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public int CreateNewContact(Entities.Contact contact)
        {
            try
            {
                int createdContactId = new DataLayer.Contact().CreateNewRecord(contact);
                return createdContactId;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                throw ex;
            }
        }

        public List<Entities.Contact> GetAllContacts()
        {
            try
            {
                
                List<Entities.Contact> data = new DataLayer.Contact().GetAllRecords();
                log.Info("Getting specific column values from database table");

                return data;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                throw new Exception("There was an error in extracting required data.");
            }
        }

        public Entities.Contact GetContactDetails(int id)
        {
            try
            {
                Entities.Contact contactData = new DataLayer.Contact().GetRecord(id);
                log.Debug($"Getting attributes of contact with id = {id} from database table");
                return contactData;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                throw ex;
            }
        }

        public int EditContact(Entities.Contact contact, int id)
        {
            try
            {
                int rowsAffected = new DataLayer.Contact().EditRecord(contact, id);
                log.Info("Edited values are being sent to DataLayer for updating");
                return rowsAffected;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                throw ex;
            }
        }

        public void DeleteAllData()
        {
            try
            {
                new DataLayer.Contact().DeleteAllRecords();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                throw ex;
            }
        }

        public void DeleteContact(int id)
        {
            try
            {
                new DataLayer.Contact().DeleteRecord(id);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                throw ex;
            }
        }

        public List<string> OrganizationList()
        {
            try
            {
                //01 Getting all contact values from database
                List<Entities.Contact> allContacts = new DataLayer.Contact().GetAllRecords();
                List<string> orgColumnValues = new List<string>();

                //02 Getting unique CompanyName values and storing them into new List
                log.Info("Getting unique CompanyName values from List of Contacts and storing them into List");
                for(int i = 0; i < allContacts.Count; i++)
                {
                    if(!orgColumnValues.Contains(allContacts[i].CompanyName) && allContacts[i].CompanyName != "")
                        orgColumnValues.Add(allContacts[i].CompanyName);
                }

                return orgColumnValues;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                throw ex;
            }
        }
    }
}
