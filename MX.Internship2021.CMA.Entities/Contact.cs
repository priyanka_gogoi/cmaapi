﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static MX.Internship2021.CMA.ExceptionLayer.ContactExceptions;

namespace MX.Internship2021.CMA.Entities
{
    public class Contact
    {
        private DateTime dob;
        public int Id { get; set; }
        public string Name { get; set; }
        public int PhoneCode { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public DateTime DateOfBirth { 
            get {
                return dob;
            } 
            set
            {
                dob = Convert.ToDateTime(value);
            } 
        }
        public string Year { get; set; }
        public string SocialName { get; set; }
        public string SocialHandle { get; set; }
        public string Relationship { get; set; }
        public byte[] ContactImage { get; set; }
        public string CompanyName { get; set; }
        public string Position { get; set; }

        public void Validate()
        {
            if (Name == "" || PhoneNo == "" || Email == "")
            {
                throw new MandatoryFieldsEmptyException("Name, Phone Number and Email fields cannot be empty.");
            }

            Regex re = new Regex(@"(^[0-9]{10}$)");
            if (!re.IsMatch(PhoneNo.Trim()))
            {
                throw new WrongPhoneNumberFormatException("Phone Number added is either in wrong format or extra characters are present. Format is \"XXXXXXXXXX\"");
            }

            re = new Regex(@"([a-z0-9]+@[a-z]+\.[a-z]{2,3}$)");
            if (!re.IsMatch(Email))
            {
                throw new InvalidEmailException("Email added is in the wrong format. Please enter a valid Email.");
            }

            if ((CompanyName != "" && Position == "") || (CompanyName == "" && Position != ""))
            {               
                throw new IncompleteCompanyDetailsException("Company Name and Position fields are both to be added to complete Company Details. Leave the textboxes empty if you intend to NOT enter Company Details.");
            }
        }

        public List<Contact> GetListFromDataTable(DataTable dt)
        {
            List<Contact> contactList = new List<Contact>();
            for(int i = 0; i<dt.Rows.Count; i++)
            {
                Contact contact = new Contact
                {
                    Id = Convert.ToInt32(dt.Rows[i]["Id"]),
                    Name = dt.Rows[i]["name"].ToString(),
                    PhoneCode = Convert.ToInt32(dt.Rows[i]["phoneCode"]),
                    PhoneNo = dt.Rows[i]["phoneNo"].ToString(),
                    Email = dt.Rows[i]["email"].ToString(),
                    Address = dt.Rows[i]["address"].ToString(),
                    City = dt.Rows[i]["city"].ToString(),
                    Country = dt.Rows[i]["country"].ToString(),
                    DateOfBirth = Convert.ToDateTime(dt.Rows[i]["dateofbirth"]),
                    SocialName = dt.Rows[i]["socialName"].ToString(),
                    SocialHandle = dt.Rows[i]["socialHandle"].ToString(),
                    Relationship = dt.Rows[i]["relationship"].ToString(),
                    Year = dt.Rows[i]["year"].ToString(),
                    CompanyName = dt.Rows[i]["companyName"].ToString(),
                    Position = dt.Rows[i]["position"].ToString(),
                    ContactImage = (byte[])dt.Rows[i]["contactImage"]
                };
                contactList.Add(contact);
            }
            return contactList;
        }
    }
}
