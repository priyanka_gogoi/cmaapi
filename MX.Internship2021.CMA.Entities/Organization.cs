﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MX.Internship2021.CMA.Entities
{
    public class Organization
    {
        private static List<string> organizationList = new List<string>();
        public void AddOrganization(string newOrg)
        {
            if(newOrg != "")
                organizationList.Add(newOrg);
        }
        public List<string> GetOrganizationList()
        {
            return organizationList;
        }
    }
}
